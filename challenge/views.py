from django.http import HttpResponse
from django.template import Context, loader

from challenge.models import Product

# Create your views here.
def index(request):
    product_list = Product.objects.order_by('-clicks')[:12]
    template = loader.get_template('challenge/index.html')
    context = Context({
        'product_list': product_list,
    })
    return HttpResponse(template.render(context))

def detail(request, product_id):
    p = Product.objects.get(id=product_id)
    p.clicks = p.clicks + 1
    p.save()
    return HttpResponse("Thanks for clicking!")
