from django.conf.urls import patterns, url

from challenge import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^(?P<product_id>\d+)/$', views.detail, name='details'),
)
